# Train API Mongoose OS (JavaScript)

## Overview

An ESP8226 scrap [Pierre Grimaud API](https://api-ratp.pierre-grimaud.fr/v3/documentation)
and print result on SSD1306 screen.

Can configure Wifi and Station to scrap via an Embedded Web Server

## Hardware

- ESP8266 (Chinesse LoLin NodeMCU)
- SSD1306 (Oled Screen)

## Software

- [Mongoose OS](https://github.com/cesanta/mongoose-os)
- [SSD1306 Adafruit Library](https://github.com/mongoose-os-libs/arduino-adafruit-ssd1306)
- Was inspired by the [Wifi-Setup-Web](https://github.com/mongoose-os-apps/wifi-setup-web) project

## How to install this app

- Install and start [mos tool](https://mongoose-os.com/software.html)
- Switch to the Project page, find and import this app, build and flash it:

<p align="center">
  <img src="https://mongoose-os.com/images/app1.gif" width="75%">
</p>
