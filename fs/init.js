/*
 * Copyright (c) 2018 Jean LE QUELLEC
 * 
 *
 * This example demonstrates how to use mJS http_client http_server Adafruit_SSD1306
 * rpc_handlers and wifi library API.
 */

// Load Mongoose OS API
load('api_timer.js');
load('api_arduino_ssd1306.js');
load('api_http.js');
load('api_rpc.js');
load('api_config.js');

// Initialize Adafruit_SSD1306 library (I2C)
let d = Adafruit_SSD1306.create_i2c(16 /* RST GPIO */, Adafruit_SSD1306.RES_128_64);

// Initialize the display.
d.begin(Adafruit_SSD1306.SWITCHCAPVCC, 0x3C, true /* reset */);
d.display();

// variables to handle API request
let hostAPI = "https://api-ratp.pierre-grimaud.fr/v3/";
let apiObj = {
  "transportMode" : Cfg.get('app.transportMode'), 
  "lineNumber" : Cfg.get('app.lineNumber'), 
  "station" : Cfg.get('app.station'), 
  "direction" : Cfg.get('app.direction')
};

let sched = 0; // counter for api
let yPos = 1; // position on the screen
let resultedAPI; // variable to handle resulted JSON  
let request = 'schedules/'+apiObj.transportMode+'/'+apiObj.lineNumber+'/'+apiObj.station+'/'+apiObj.direction+'?_format=json';

let showStr = function(d, str, position) {
  //print('Show Str: ' + str);
  d.setTextSize(2);
  d.setTextColor(Adafruit_SSD1306.WHITE);
  d.setCursor(1, position);
  d.write(str);
  d.display();
};

Timer.set(10000 /* milliseconds */, Timer.REPEAT, function() {
  parseApi(apiObj);
}, null);

RPC.addHandler('API.Get', function(args) {  
  //print('API Get: ' + JSON.stringify(apiObj));
  return resultedAPI;
});

RPC.addHandler('API.Set', function(args) {
  //print('API Set: ' + JSON.stringify(args));
  apiObj.transportMode = args.transportMode;
  apiObj.station = args.station;
  apiObj.lineNumber = args.lineNumber;
  apiObj.direction = args.direction;
  parseApi(apiObj);
  return apiObj;
});

let parseApi = function (Obj) {
  //print('Parse API: ' + JSON.stringify(Obj));
  d.clearDisplay();
  yPos = 1;
  request = 'schedules/'+Obj.transportMode+'/'+Obj.lineNumber+'/'+Obj.station+'/'+Obj.direction+'?_format=json'; 
  HTTP.query({
    url: hostAPI + request,
    headers: {
      'GET':  request + 'HTTP/1.1',
    },
    success: function(res) {
      resultedAPI = JSON.parse(res)['result'];
      //print('Resulted API: ' + JSON.stringify(resultedAPI));
      for (sched = 0; sched < resultedAPI.schedules.length; sched++){
        if(resultedAPI.schedules[sched].message[0] !== 'T' &&  resultedAPI.schedules[sched].message[0] !== 'S'){
          if(yPos <= 61){
            showStr(d,resultedAPI.schedules[sched].destination.slice(0,4) + ' ' + resultedAPI.schedules[sched].message.slice(0,5),yPos);
            yPos+=16;
          }
          else{
            yPos=1;
            return yPos;
          }
        }
      }
    },
    error: function(err) {
      print('error: ');
      print(JSON.stringify(err));
      showStr(d,'Erreur API',1);
    }
  });
};
